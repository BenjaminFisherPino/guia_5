public class Profesor extends Persona implements Interfaz{

	//ATRIBUTOS
	private int año_incorporacion;
	
	Profesor(String nombre, String apellidos, String estado, int id, int año){
		super(nombre,apellidos, estado,id);
		this.año_incorporacion = año;
		
	}
	
	public void informacion(){
		System.out.println("Nombre: " + this.nombre + "\nApellidos: " + this.apellidos +
		           "\nID: " + this.id + "\nAño de ingreso: " + this.año_incorporacion + "\n");
	}
}
