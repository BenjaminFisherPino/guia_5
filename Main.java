import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		String str;
		int respuesta, opcion, cont, libro,revista;
		Biblioteca b = new Biblioteca();
		Scanner sc = new Scanner(System.in);
		respuesta = 1;
		
		Libro l1 = new Libro(1012930,"El diario de Ana Frank",1944,false,"Ana Frank");
		Libro l2 = new Libro(1391239,"Eine Kurze Geschichte Der Zeit",1988,false,"Stephen W.Hawking");
		Libro l3 = new Libro(3019394,"One Piece",1997,false,"Eiichiro Oda");
		Libro l4 = new Libro(1230994,"El Kybalion",0,false,"SN");
		Libro l5 = new Libro(8172498,"The Black Cat",1943,false,"Edgar Alan Poe");
		
		Revista r1 = new Revista(1091092,"El increible Hombre Araña",1963,false,"comic",1,false,"Stan Lee");
		Revista r2 = new Revista(1023094,"Condorito",2005,false,"comic",12,false,"Pepo");
//		Revista r3 = new Revista();
//		Revista r4 = new Revista();
//		Revista r5 = new Revista();
		
		b.add_libros(l1);
		b.add_libros(l2);
		b.add_libros(l3);
		b.add_libros(l4);
		b.add_libros(l5);
		
		b.add_revistas(r1);
		b.add_revistas(r2);
		
		System.out.println("Bienvenido a la biblioteca!\n\n");
		
		//CICLO PRINCIPAL
		while(respuesta == 1) {
			System.out.println("Presione 1 para ver el catalogo de libros\nPresione 2 para ver el catalogo de revistas\n" + 
		                       "Presione 3 para arrendar un libro\nPresione 4 para arrendar una revista\n" + 
					           "Presione 5 para devolver un libro\nPresione 6 para devolver una revista\n");
			
			opcion = sc.nextInt();
			
			//CATALOGO LIBROS
			if(opcion == 1) {
				for(Libro l: b.get_libros()) {
					l.muestra_detalle();
					System.out.println("\n");
				}
			}
			
			//CATALOGO REVISTAS
			else if(opcion == 2) {

				for(Revista r: b.get_revistas()) {
					r.muestra_detalle();
				}
			}
			
			//ARRENDAR LIBRO
			else if(opcion == 3) {
				System.out.println("Ingrese el indice del libro a arrendar");
				libro = sc.nextInt()-1;
				if(b.get_libros().get(libro).get_estado()) {
					System.out.println("El libro no se encuentra disponible...\n");
				}
				else {
					b.get_libros().get(libro).prestar();
					b.get_lprestados().add(b.get_libros().get(libro));
				}
			}
				
			//ARRENDAR REVISTA
			else if(opcion == 4) {
				System.out.println("Ingrese el indice de la revista a arrendar");
				revista = sc.nextInt()-1;
				if(b.get_revistas().get(revista).get_estado()){
					System.out.println("La revista no se encuentra disponible...\n");
				}
				else {
					//EN CASO QUE ESTE CADUCADA, SE REGALA(ELIMINA)
					if(b.get_revistas().get(revista).get_caducacion()) {
						System.out.println("Se regalo la revista " +  b.get_revistas().get(revista).get_nombre());
						b.get_revistas().remove(revista);
					}
					
					//CASO CONTRARIO, PASA A ESTAR PRESTADA
					else {
						b.get_revistas().get(revista).prestar();
						b.get_rprestadas().add(b.get_revistas().get(revista));
					}
				}
 			}
			
			//DEVOLVER LIBRO
			else if(opcion == 5) {
				cont = 1;
				if(b.get_lprestados().size() == 0) {
					System.out.println("Usted no debe ningun libro...");
				}
				else {
					System.out.println("Esta es su lista de libros pendientes\nIngrese el indice del libro a devolver");
					for(Libro l: b.get_lprestados()) {
						System.out.println(cont + ") " + l.get_nombre());
						cont++;
					}
					libro = sc.nextInt()-1;
					b.get_libros().get(libro).devolver();
					b.get_lprestados().remove(libro);
				}
			}
			
			//DEVOLVER REVISTA
			else if(opcion == 6) {
				cont = 1;
				System.out.println("Esta es su lista de revistas pendientes\nIngrese el indice de la revista a devolver");
				for(Revista r: b.get_rprestadas()) {
					System.out.println(cont + ") " + r.get_nombre());
					cont++;
				}
				revista = sc.nextInt()-1;
				if(b.get_rprestadas().get(revista).get_eliminar()) {
					b.get_revistas().remove(revista);
					b.get_rprestadas().remove(revista);
				}
				else {
					b.get_rprestadas().get(revista).devolver();
					b.get_rprestadas().remove(revista);
				}
			}
			
			//OPCION INVALIDA
			else {
				System.out.println("Asegurece de ingresar una opcion valida...");
			}
			
			//CICLO VERIFICADOR DE LIBROS Y REVISTAS PRESTADAS
			if(b.get_lprestados().size()>0) {
				for(Libro l:b.get_lprestados()) {
					l.prestado();
				}
			}
			
			if(b.get_rprestadas().size()>0) {
				for(Revista r:b.get_rprestadas()) {
					r.prestado();
					
				}
			}
			
		}
	}
}
