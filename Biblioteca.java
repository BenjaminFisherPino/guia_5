import java.util.ArrayList;

public class Biblioteca {
	
	//Atributos
	private ArrayList<Libro> libros = new ArrayList<Libro>();
	private ArrayList<Libro> lprestados = new ArrayList<Libro>();
	private ArrayList<Revista> revistas = new ArrayList<Revista>();
	private ArrayList<Revista> rprestadas = new ArrayList<Revista>();
	
	//METODOS
	public void add_libros(Libro a) {
		libros.add(a);
	}
	
	public void add_revistas(Revista a) {
		revistas.add(a);
	}
	
	public void add_lpretado(Libro a) {
		lprestados.add(a);
	}
	
	public void add_rprestada(Revista a) {
		rprestadas.add(a);
	}
	
	public ArrayList<Libro> get_libros() {
		return this.libros;
	}
	
	public ArrayList<Revista> get_revistas(){
		return this.revistas;
	}
	
	public ArrayList<Libro> get_lprestados() {
		return this.lprestados;
	}
	
	public ArrayList<Revista> get_rprestadas(){
		return this.rprestadas;
	}
}
