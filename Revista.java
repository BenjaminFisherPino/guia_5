import java.time.Duration;
import java.time.LocalDateTime;

public class Revista extends Formato implements Controlador{

	//ATRIBUTOS
	private LocalDateTime fecha;
	private int edicion;
	private boolean caducacion, eliminar;
	private String genero,temp;
	
	//CONSTRUCTOR
	Revista(int a, String b, int c, boolean d, String e, int f, boolean g, String h){
		super(a,b,c,d,h);
		this.genero = e;
		this.edicion = f;
		this.caducacion = g;
		this.eliminar = false;
	}
	
	//METODOS
	public void prestar() {
		this.fecha = LocalDateTime.now();
		set_estado(true);
	}
	
	public void prestado() {
		if(this.genero.equals("comic")) {
			if(Duration.between(fecha, LocalDateTime.now()).toDays() > 1) {
				System.out.println("Se pedio la revista" + get_nombre());
				this.eliminar = true;
				}
		}
		
		else if (this.genero.equals("cientifica")) {
			if(Duration.between(fecha, LocalDateTime.now()).toDays() > 2) {
				System.out.println("Se pedio la revista" + get_nombre());
				this.eliminar = true;
			}
		}
		
		else if(this.genero.equals("deportiva")) {
			if(Duration.between(fecha, LocalDateTime.now()).toDays() > 2) {
				System.out.println("Se pedio la revista" + get_nombre());
				this.eliminar = true;
			}
		}
		//GASTRONOMICA
		else {
			if(Duration.between(fecha, LocalDateTime.now()).toDays() > 3) {
				System.out.println("Se pedio la revista" + get_nombre());
				this.eliminar = true;
			}
		}
	}
	
	public void devolver() {
		set_estado(false);
		System.out.println("Gracias por devolver la revista " + get_nombre());
	}
	
	public Boolean get_eliminar() {
		return this.eliminar;
	}
	
	public Boolean get_caducacion() {
		return this.caducacion;
	}
	
	public void muestra_detalle() {
		if(get_estado()) {
			this.temp = "Prestado";
		}
		else {
			this.temp = "Disponible";
		}
		System.out.println('"'+ get_nombre() + " - " + get_autor() + ", " + get_anio() + ", ISBN: " +
	                       get_codigo() + " - " + this.temp + ", edicion: " + this.edicion + "\n");
	
	}
}
