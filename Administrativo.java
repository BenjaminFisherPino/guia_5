public class Administrativo extends Persona implements Interfaz{

	//ATRIBUTOS
	private int año_incorporacion;
	private String seccion;
	
	//CONSTRUCTOR
	Administrativo(String nombre, String apellidos, String estado, String seccion, int id, int año){
		super(nombre, apellidos, estado ,id);
		this.año_incorporacion = año;	
		this.seccion = seccion;
	}
	
	public void informacion(){
		System.out.println("Nombre: " + this.nombre + "\nApellidos: " + this.apellidos +
		           "\nID: " + this.id + "\nAño de ingreso: " + this.año_incorporacion + "\nSeccion: " + this.seccion + "\n");
	}
}
