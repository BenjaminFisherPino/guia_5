import java.time.Duration;
import java.time.LocalDateTime;

public class Libro  extends Formato implements Controlador{

	//ATRIBUTOS
	private LocalDateTime fecha;
	private int deuda;
	private String temp;
	
	//CONSTRUCTOR
	Libro(int a, String b, int c, boolean d, String e){
		super(a,b,c,d,e);
		this.deuda = 0;
	}
	
	//METODOS
	public void prestar() {
		this.fecha = LocalDateTime.now();
		set_estado(true);
	}
	
	public void prestado() {
		Duration duration = Duration.between(this.fecha, LocalDateTime.now());
		if(Duration.between(this.fecha, LocalDateTime.now()).toDays() > 5) {
			this.deuda = (int) (1290 * (duration.toDays() - 5));
		}
	}
	
	public int get_deuda() {
		return this.deuda;
	}
	
	public void devolver() {
		if(this.deuda == 0) {
			System.out.println("Gracias por devolver el libro a tiempo!");
		}
		else {
			System.out.println("Usted debe una deuda de " + this.deuda + " pesos");
			System.out.println("Para la proxima asegurece de devolver el libro a tiempo");
		}
		set_estado(false);
	}
	
	public void muestra_detalle() {
		if(get_estado()) {
			this.temp = "Prestado";
		}
		else {
			this.temp = "Disponible";
		}
		System.out.println('"'+ get_nombre() + " - " + get_autor() + ", " + get_anio() + ", ISBN: " +
	                       get_codigo() + " - " + this.temp);
	}
}
