public class Estudiante extends Persona implements Interfaz{

	//ATRIBUTOS
	private int año_actual;
	
	Estudiante(String nombre, String apellidos,String estado, int id, int año){
		super(nombre,apellidos,estado,id);
		this.año_actual = año;
	}
	
	public void informacion(){
		System.out.println("Nombre: " + this.nombre + "\nApellidos: " + this.apellidos +
				           "\nID: " + this.id + "\nAlumno cursando: " + this.año_actual + "\n");
	}
	
}
