public class Formato {

	//ATRIBUTOS	
	private int codigo;
	private String nombre, autor;
	private int anio;
	private boolean estado;
	
	//CONSTRUCTOR
	public Formato(int a, String b, int c, boolean d, String e){
		this.codigo = a;
		this.nombre = b;
		this.anio = c;
		this.estado = d;
		this.autor = e;
	}
	
	//METODOS
	public void set_codigo() {
		this.codigo = 00000000;
	}
	
	public int get_codigo() {
		return this.codigo;
	}
	
	public void set_nombre() {
		this.nombre = "SN";
	}
	
	public String get_nombre() {
		return this.nombre;
	}
	
	public void set_anio() {
		this.anio = 0;
	}
	
	public int get_anio() {
		return this.anio;
	}
	
	public void set_estado(boolean a) {
		this.estado = a;
	}
	
	public boolean get_estado() {
		return this.estado;
	}
	
	public void set_autor() {
		this.autor = "SN";
	}
	
	public String get_autor() {
		return this.autor;
	}
	
	public void muestra_detalle() {
		
	}
}
